import { LOGIN } from "../constants";
export function authRedux(user) {
  return {
    type: LOGIN,
    payload: user,
  };
}
