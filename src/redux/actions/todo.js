import { ADD_TODO, UPDATE_TODO, DELETE_TODO, ADD_TODOS } from "../constants";

export function addTodosRedux(todos) {
  return {
    type: ADD_TODOS,
    payload: todos,
  };
}
export function addTodoRedux(todo) {
  return {
    type: ADD_TODO,
    payload: todo,
  };
}
export function updateTodoRedux(todo) {
  return {
    type: UPDATE_TODO,
    payload: todo,
  };
}

export function deleteTodoRedux(id) {
  return {
    type: DELETE_TODO,
    payload: id,
  };
}
