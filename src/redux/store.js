import { createStore, applyMiddleware, combineReducers } from "redux";
import thunk from "redux-thunk";

import { todoReducer } from "./reducers/todo.js";
import { userReducer } from "./reducers/user.js";

const rootReducer = combineReducers({
  todoReducer,
  userReducer,
});
const store = createStore(rootReducer, applyMiddleware(thunk));
export default store;
