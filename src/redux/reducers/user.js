import { LOGIN } from "../constants";

const initialState = {
  user: {},
};
export const userReducer = (state = initialState, action) => {
  switch (action.type) {
    case LOGIN:
      return { ...state, user: action.payload.user };

    default:
      return state;
  }
};
