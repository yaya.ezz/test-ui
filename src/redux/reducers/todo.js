import { ADD_TODO, UPDATE_TODO, DELETE_TODO, ADD_TODOS } from "../constants";

const initialState = {
  todos: [],
};
export const todoReducer = (state = initialState, action) => {
  switch (action.type) {
    case ADD_TODOS:
      return { ...state, todos: action.payload };
    case ADD_TODO:
      const { title, id } = action.payload;
      return {
        ...state,
        todos: [...state.todos, action.payload],
      };
    case UPDATE_TODO:
      const { isCompleted } = action.payload;
      return state.todos.map((todo) => {
        if (todo.id === id) {
          return { ...todo, isCompleted: isCompleted };
        }
        return todo;
      });
    case DELETE_TODO:
      return state.todos.filter((todo) => todo.id !== action.payload.id);

    default:
      return state;
  }
};
