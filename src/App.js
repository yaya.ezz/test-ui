import "./App.css";
import { Routes, Route, Navigate } from "react-router-dom";
import Header from "./components/Layout/Header";
import isAuthenticated from "./utils/isAuthenticated";
import Auth from "./pages/Auth";
import Todos from "./pages/Todos";
import Container from "@material-ui/core/Container";

function App() {
  return (
    <div className="App">
      <Header />
      <Container maxWidth="sm">
        <Routes>
          <Route
            exact
            path="/"
            element={
              isAuthenticated ? <Todos /> : <Navigate to="/login" replace />
            }
          />
          <Route
            path="todos"
            element={
              isAuthenticated ? <Todos /> : <Navigate to="/login" replace />
            }
          />
          <Route
            path="login"
            element={!isAuthenticated ? <Auth /> : <Navigate to="/" replace />}
          />
          <Route
            path="singup"
            element={!isAuthenticated ? <Auth /> : <Navigate to="/" replace />}
          />
        </Routes>
      </Container>
    </div>
  );
}

export default App;
