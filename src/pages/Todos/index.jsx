import React, { useEffect } from "react";
import TodoForm from "../../components/forms/TodoForm";
import { Check } from "@material-ui/icons";
import { makeStyles, Typography, Grid } from "@material-ui/core";
import { addTodoRedux, addTodosRedux } from "../../redux/actions/todo.js";
import { useDispatch, useSelector } from "react-redux";
import { CREATE_TODO_Q, LIST_TODO_Q } from "../../graphql/Queries.js";
import { useMutation, useLazyQuery } from "@apollo/react-hooks";

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    margin: theme.spacing(5),
    textAlign: "left",
    padding: theme.spacing(1),
  },
  paper: {
    margin: theme.spacing(8),
    color: theme.palette.text.secondary,
  },
}));

function Todos() {
  const classes = useStyles();
  const dispatch = useDispatch();

  const [listTodo] = useLazyQuery(LIST_TODO_Q, {
    fetchPolicy: "network-only",
    onCompleted: (data) => {
      dispatch(addTodosRedux(data.listTodo));
    },
  });
  const { todos } = useSelector((state) => state.todoReducer);

  useEffect(() => {
    listTodo();
  }, []);

  const [createTodo] = useMutation(CREATE_TODO_Q, {
    onCompleted: async (data) => {
      dispatch(addTodoRedux(data.createTodo));
    },
    onError: (error) => console.error("Error login", error),
  });

  const hundelAddTodo = async (newTodo) => {
    await createTodo({ variables: { title: newTodo } });
  };

  return (
    <div className={classes.root}>
      <Grid container>
        <Grid item xs={6}>
          <Check color="primary" style={{ fontSize: 40, fontWeight: "bold" }} />

          <Typography variant="h5" style={{ margin: 15 }} display="block">
            Todo List
          </Typography>

          <TodoForm hundelAddTodo={hundelAddTodo} todos={todos ? todos : []} />
        </Grid>
      </Grid>
    </div>
  );
}

export default Todos;
