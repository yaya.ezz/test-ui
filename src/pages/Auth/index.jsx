import React, { useState, useEffect } from "react";
import AuthForm from "../../components/forms/AuthForm";
import { Check } from "@material-ui/icons";
import { makeStyles, Typography, Grid } from "@material-ui/core";
import { useLocation } from "react-router-dom";
import { authRedux } from "../../redux/actions/user.js";
import { useDispatch } from "react-redux";
import { LOGIN_Q, SINGUP_Q } from "../../graphql/Queries.js";
import { useMutation } from "@apollo/react-hooks";

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    margin: theme.spacing(5),
    textAlign: "left",
    padding: theme.spacing(1),
  },
  paper: {
    margin: theme.spacing(8),
    color: theme.palette.text.secondary,
  },
}));

function Auth() {
  let location = useLocation();
  const classes = useStyles();
  const [auth, setAuth] = useState("login");
  const dispatch = useDispatch();

  useEffect(() => {
    let path = location.pathname.slice(1);
    setAuth(path);
  }, [location]);

  const [login] = useMutation(LOGIN_Q, {
    onCompleted: async (data) => {
      await saveAuth(data.login);
    },
    onError: (error) => console.error("Error login", error),
  });

  const [signUp] = useMutation(SINGUP_Q, {
    onCompleted: async (data) => {
      await saveAuth(data.signUp);
    },
    onError: (error) => console.error("Error singup", error),
  });

  const hundelLogin = async (email, password, name = null) => {
    if (auth === "login") {
      await login({ variables: { email, password } });
    } else {
      await signUp({ variables: { email, password, name } });
    }
  };

  const saveAuth = async (user) => {
    localStorage.setItem("token", user.token);
    dispatch(authRedux(user));
    window.location.href = "/todos";
  };
  return (
    <div className={classes.root}>
      <Grid container>
        <Grid item xs={6}>
          <Check color="primary" style={{ fontSize: 40, fontWeight: "bold" }} />

          <Typography variant="h5" style={{ margin: 15 }} display="block">
            {auth === "login" ? "Welcome Back!" : "Welcome!"}
          </Typography>
          <Typography
            variant="subtitle1"
            style={{ marginLeft: 15 }}
            gutterBottom
            display="block"
            color="initial"
          >
            {auth === "login"
              ? "Login To Continue"
              : "Sing Up to start using Simple Do today "}
          </Typography>
          <AuthForm hundelAuth={hundelLogin} auth={auth} setAuth={setAuth} />
        </Grid>
      </Grid>
    </div>
  );
}

export default Auth;
