import React, { useState } from "react";

import {
  TextField,
  makeStyles,
  Checkbox,
  FormControlLabel,
  FormGroup,
  Button,
} from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
  root: {
    "& > *": {
      margin: theme.spacing(1),
      width: "40ch",
    },
  },
}));
function TodoForm({ hundelAddTodo, todos }) {
  const [todo, setTodo] = useState("");
  const classes = useStyles();
  const handleSubmit = (e) => {
    e.preventDefault();
    if (todo) {
      hundelAddTodo(todo);
      setTodo("");
    }
  };

  const handleChange = (event) => {};
  return (
    <>
      <form className={classes.root} noValidate onSubmit={handleSubmit}>
        <TextField
          name="todo"
          id="standard-basic"
          label="Add a new todo"
          onChange={(e) => setTodo(e.target.value)}
          value={todo}
        />

        <Button
          style={{
            display: "none",
          }}
          type="submit"
        >
          jj
        </Button>
      </form>
      <FormGroup row>
        {todos.map((todo) => (
          <FormControlLabel
            key={todo.id}
            control={
              <Checkbox
                key={todo.id}
                checked={true}
                onChange={handleChange}
                name="checkedB"
                color="primary"
              />
            }
            label={todo.title}
          />
        ))}
      </FormGroup>
    </>
  );
}

export default TodoForm;
