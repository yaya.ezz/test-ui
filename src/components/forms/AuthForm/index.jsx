import React, { useState } from "react";

import { Link, Button, TextField, makeStyles } from "@material-ui/core";
import { useNavigate } from "react-router-dom";

const useStyles = makeStyles((theme) => ({
  root: {
    "& > *": {
      margin: theme.spacing(2),
      width: "40ch",
    },
  },
}));
function AuthForm({ hundelAuth, auth }) {
  let navigate = useNavigate();
  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const classes = useStyles();
  const handleSubmit = () => {
    if (auth === "login") {
      if ((email, password)) {
        hundelAuth(email, password);
      }
    } else {
      if ((email, password, name)) {
        hundelAuth(email, password, name);
      }
    }
  };
  const changeAuth = () => {
    auth === "login" ? navigate("/singup") : navigate("/login");
  };
  return (
    <form className={classes.root} noValidate>
      {auth !== "login" && (
        <TextField
          name="email"
          id="standard-basic"
          label="Name"
          onChange={(e) => setName(e.target.value)}
          value={name}
        />
      )}
      <TextField
        name="email"
        id="standard-basic"
        label="Email"
        onChange={(e) => setEmail(e.target.value)}
        value={email}
      />
      <TextField
        id="standard-basic"
        label="Passord"
        type="password"
        onChange={(e) => setPassword(e.target.value)}
        value={password}
      />
      <Link onClick={changeAuth} color="inherit">
        {auth === "login"
          ? "Don't hav an account? Sing Up"
          : "Have an account? Login"}
      </Link>

      <Button
        style={{
          maxWidth: "400x",
          maxHeight: "35px",
          minWidth: "320px",
          minHeight: "35px",
        }}
        variant="contained"
        color="primary"
        size="large"
        onClick={handleSubmit}
      >
        {auth === "login" ? "Login" : "Sing Up "}
      </Button>
    </form>
  );
}

export default AuthForm;
