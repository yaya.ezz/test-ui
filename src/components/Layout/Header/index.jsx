import React from "react";
import {
  AppBar,
  IconButton,
  Toolbar,
  List,
  ListItem,
  ListItemText,
  makeStyles,
  Container,
  Button,
} from "@material-ui/core";
import { Home } from "@material-ui/icons";
import { Link } from "react-router-dom";
import isAuthenticated from "../../../utils/isAuthenticated";
const usedStyles = makeStyles({
  navbarDisplayFlex: {
    display: "flex",
    justifyContent: "space-between",
  },
  navDisplayFlex: {
    display: "flex",
    justifyContent: "space-between",
  },
  linkText: {
    textDecoration: "none",
    textTransform: "uppercase",
    width: "90px",
    color: "white",
  },
});

const navLinks = isAuthenticated
  ? [
      { title: "home", path: "/" },
      { title: "todo list", path: "/todos" },
    ]
  : [
      { title: "home", path: "/" },
      { title: "login", path: "/login" },
      { title: "sing up", path: "/singup" },
    ];
const Hearder = () => {
  const classes = usedStyles();
  const hundelLogout = () => {
    localStorage.clear();
    window.location.href = "/";
  };
  return (
    <>
      <AppBar position="static">
        <Toolbar>
          <Container maxWidth="lg" className={classes.navbarDisplayFlex}>
            <IconButton edge="start" color="inherit" aria-label="home">
              <Link to="/"></Link>
              <Home fontSize="large" />
            </IconButton>
            <List
              component="nav"
              aria-labelledby="main navigation"
              className={classes.navDisplayFlex}
            >
              {navLinks.map(({ title, path }) => (
                <ListItem button key={title} component={Link} to={path}>
                  <ListItemText className={classes.linkText} primary={title} />
                </ListItem>
              ))}
              {isAuthenticated && (
                <ListItem button>
                  <Button onClick={hundelLogout} className={classes.linkText}>
                    log out
                  </Button>
                </ListItem>
              )}
            </List>
          </Container>
        </Toolbar>
      </AppBar>
    </>
  );
};

export default Hearder;
