import gql from "graphql-tag";

export const SINGUP_Q = gql`
  mutation SignUp($name: String!, $email: String!, $password: String!) {
    signUp(name: $name, email: $email, password: $password) {
      id
      name
      email
      token
    }
  }
`;
export const LOGIN_Q = gql`
  mutation Login($email: String!, $password: String!) {
    login(email: $email, password: $password) {
      token
      id
      name
    }
  }
`;

export const CREATE_TODO_Q = gql`
  mutation CreateTodo($title: String!) {
    createTodo(title: $title) {
      id
      title
      isCompleted
    }
  }
`;
export const UPDATE_TODO_Q = gql`
  mutation Mutation($id: Number) {
    updateTodo(id: $id) {
      title
      isCompleted
      id
    }
  }
`;

export const DELETE_TODO_Q = gql`
  mutation DeleteTodo($id: Number!) {
    deleteTodo(id: $id)
  }
`;

export const LIST_TODO_Q = gql`
  query Query {
    listTodo {
      id
      title
      isCompleted
    }
  }
`;
