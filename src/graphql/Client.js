import { setContext } from "apollo-link-context";
import { onError } from "apollo-link-error";
import { ApolloClient, InMemoryCache, HttpLink } from "@apollo/client";
const apiUrl = process.env.REACT_APP_API_URL;
const httpLink = new HttpLink({
  uri: apiUrl,
});

const errorLink = onError(({ graphQLErrors, networkError }) => {
  console.log(networkError, graphQLErrors);
});

const authLink = setContext(async (_, { headers }) => {
  const token = localStorage.getItem("token");
  return {
    headers: {
      ...headers,
      authorization: token || null,
    },
  };
});
const client = new ApolloClient({
  link: authLink.concat(httpLink, errorLink),
  cache: new InMemoryCache(),
});
export default client;
